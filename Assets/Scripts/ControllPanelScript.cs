﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControllPanelScript : MonoBehaviour
{
    // Start is called before the first frame update

    public Animator controllerAnimator;

    void Start()
    {
        controllerAnimator = GetComponent<Animator>();
    }

    public void Enter()
    {
        controllerAnimator.Play("Base Layer.Enter", 0,0);
        Debug.Log("StartControllsEnterAnim");
    }


    public void Exit()
    {
        controllerAnimator.Play("Base Layer.Exit", 0, 0);
        
    }





}
