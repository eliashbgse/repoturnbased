﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{

    public float enemyHp;
    public float enemyStamina;
    public float enemyDmg;
    bool isDead;
    public Animator enemyAnim;
    TextHandler textHandler;
    [SerializeField]
    GameObject particlePrefab;



    GameManager gameManager;
    GameObject gameMangerGO;
    // Start is called before the first frame update
    private void Start()
    {
        gameMangerGO = GameObject.FindGameObjectWithTag("GameController");
        gameManager = gameMangerGO.GetComponent<GameManager>();
        gameManager.enemyHP = enemyHp;
        textHandler = gameMangerGO.transform.GetChild(0).gameObject.GetComponent<TextHandler>();
        enemyAnim = GetComponent<Animator>();
    }
    private void Update()
    {
        if (enemyHp <= 0)
        {
            gameManager.PlayerWon();
        }
    }

    public void ChooseMove()
    {
        Debug.Log("ChooseMove1");
        StartCoroutine(EnemyAttack());
        
    }

    void Attack()
    {
        StartCoroutine(EnemyAttack());
    }
    void Block()
    {

    }
    void Heal()
    {

    }
    bool isCrit()
    {
        if (UnityEngine.Random.Range(0, 0.5f) == 0.5)
        { return true; }
        else { return false; }
    }
    IEnumerator EnemyAttack()
    {
        if (!gameManager.playerBlocked) {
            enemyAnim.SetTrigger("EnemyAttack");

            if (isCrit())
            {
                gameManager.DmgPlayer(enemyDmg + enemyDmg / 5);
            }
            else
            {
                gameManager.DmgPlayer(enemyDmg);
            }
            yield return new WaitForSeconds(1);
            textHandler.UpdateStats();
            yield return new WaitForSeconds(3);
            enemyAnim.ResetTrigger("EnemyAttack");
            if (gameManager.playerHP <= 0)
            {
                gameManager.playerWon = false;
                gameManager.state = BattleState.Lost;
                gameManager.PlayerLost();
            }
            else
            {
                gameManager.state = BattleState.EnemyTurn;
                gameManager.PlayerTurn();
            }
        }
        else
        {
            enemyAnim.SetTrigger("EnemyAttack");
            yield return new WaitForSeconds(3);
            Debug.Log("PlayerBlocked");
            gameManager.PlayerTurn();
            enemyAnim.ResetTrigger("EnemyAttack");
        }

    }
    IEnumerator EnemyBlock()
    {
        //play animation
        if (enemyBlock())
        {
            gameManager.playerBlocked = true;
        }
        else
        {
            gameManager.playerBlocked = false;
        }
        textHandler.UpdateStats();
        yield return new WaitForSeconds(4);
        gameManager.state = BattleState.EnemyTurn;
        gameManager.PlayerTurn();

    }
    public bool enemyBlock()
    {
        if (UnityEngine.Random.Range(0, 100) > 60)
        {
            Debug.Log("BlockSucsesfullEnemy");
            return true;
        }
        else
        {
            Debug.Log("BlockFailedEnemy");
            return false;
        }
    }
    IEnumerator EnemyHeal()
    {
        enemyAnim.SetTrigger("Heal");
        yield return new WaitForSeconds(1.5f);
        SpawnHealParticles();
        enemyHp += UnityEngine.Random.Range(0, 15);
        if(enemyHp > 100)
        {
            enemyHp = 100;
        }
        enemyStamina -= 10;
        gameManager.enemyHP = enemyHp;
        gameManager.enemyStamina = enemyStamina;
        textHandler.UpdateStats();
        yield return new WaitForSeconds(2.5f);
        enemyAnim.ResetTrigger("Heal");
        gameManager.state = BattleState.PlayerTurn;
        Debug.Log("EnemyTurnOver");
        gameManager.PlayerTurn();


    }
    public void SpawnHealParticles()
    {
        GameObject particleSystem = Instantiate(particlePrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z - 1), transform.rotation);
        Destroy(particleSystem, 4);
    }





}
