﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public enum BattleState { PlayerTurn, EnemyTurn, Won, Lost, Start }
public class GameManager : MonoBehaviour
{
    // Start is called before the first frame update
    public float playerHP;
    public float enemyHP;
    public float playerStamina;
    public float enemyStamina;
    Player playerScript;
    Enemy enemyScript;
    public GameObject playerPrefab;
    public GameObject enemyPrefab;
    static GameObject player;
    static GameObject enemy;
    public Transform playerPlatform;
    public Transform enemyPlatform;
    public GameObject playerControlls;
    public Animator playerAnimator;
    public bool playerWon;
    public TextHandler texthandler;
    public GameObject TextHandlerObj;
    ControllPanelScript controllpanelscript;
    int maxClicks = 0;
    public bool playerBlocked;
    public bool enemyBlocked;

    public BattleState state;

    void Start()
    {
        state = BattleState.Start;
        StartCoroutine(SetupBattle());
    }

    IEnumerator SetupBattle()
    {
        player = GameObject.Instantiate(playerPrefab, playerPlatform);
        enemy = GameObject.Instantiate(enemyPrefab, enemyPlatform);
        playerScript = player.GetComponent<Player>();
        enemyScript = enemy.GetComponent<Enemy>();
        player.transform.position = new Vector2(playerPlatform.position.x, playerPlatform.position.y + 2.25f);
        enemy.transform.position = new Vector2(enemyPlatform.position.x, enemyPlatform.position.y + 1.75f);
        playerAnimator = player.GetComponent<Animator>();
        TextHandlerObj = transform.GetChild(0).gameObject;
        texthandler = TextHandlerObj.GetComponent<TextHandler>();
        controllpanelscript = playerControlls.GetComponent<ControllPanelScript>();
        yield return new WaitForSeconds(4);
        state = BattleState.PlayerTurn;
    }
    private void Update()
    {

    }
    public void PlayerTurn()
    {
        playerBlocked = false;
        maxClicks = 0;
        state = BattleState.PlayerTurn;
        Debug.Log("playerTurn");
        controllpanelscript.Enter();
    }
    public void EnemyTurn()
    {
        enemyBlocked = false;
        state = BattleState.EnemyTurn;
        enemyScript.ChooseMove();
    }
    public void PlayerAttackBtn()
    {

        if (maxClicks == 0)
        {
            playerScript.StartCoroutine(playerScript.PlayerAttack());
            maxClicks++;
            controllpanelscript.Exit();
        }
    }
    public void PlayerBlockBtn()
    {
        if (maxClicks == 0)
        {
            playerScript.StartCoroutine(playerScript.PlayerBlock());
            maxClicks++;
            controllpanelscript.Exit();
        }
    }
    public void PlayerHealBtn()
    {
        if (maxClicks == 0)
        {
            playerScript.StartCoroutine(playerScript.PlayerHeal());
            controllpanelscript.Exit();
            maxClicks++;
        }
    }

    public void DmgEnemy(float amount)
    {
        enemyScript.enemyHp -= amount;
        enemyHP = enemyScript.enemyHp;
    }
    public void DmgPlayer(float amount)
    {
        playerScript.playerHp -= amount;
        playerHP = playerScript.playerHp;
    }


    public void PlayerLost()
    {
        if(playerHP <= 0)
        {
            SceneManager.LoadScene("MainMenu");
        }
    }
    public void PlayerWon()
    {
        if(enemyHP <= 0)
        {
            StartCoroutine(SwtichToWin());
        }
    }
    IEnumerator SwtichToWin()
    {
        yield return new WaitForSeconds(3);
        SceneManager.LoadScene("WinScene");
    }
















}
