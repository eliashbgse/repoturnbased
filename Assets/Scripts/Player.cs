﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
    public float playerHp;
    public float playerStamina;
    public float playerDmg = 10;
    GameManager gameManager;
    GameObject gameMangerGO;
    Animator playerAnimator;
    TextHandler texthandler;
    GameObject texthandlerObj;
    [SerializeField]
    GameObject particlePrefab;

    // Start is called before the first frame update
    private void Start()
    {
        gameMangerGO = GameObject.FindGameObjectWithTag("GameController");
        gameManager = gameMangerGO.GetComponent<GameManager>();
        texthandlerObj = gameManager.TextHandlerObj;
        texthandler = gameManager.texthandler;
        playerAnimator = GetComponent<Animator>();
    }
    private void Update()
    {
        if (playerHp <= 0)
        {
            gameManager.PlayerLost();
        }
    }
    bool isCrit()
    {
        if (Random.Range(0, 0.5f) == 0.5)
        { return true; }
        else { return false; }
    }
    public IEnumerator PlayerAttack()
    {
        playerAnimator.SetTrigger("Attack");

        if (!gameManager.enemyBlocked)
        {
            if (isCrit())
            {
                gameManager.DmgEnemy(playerDmg + playerDmg / 5);
            }
            else
            {
                gameManager.DmgEnemy(playerDmg);
            }
            yield return new WaitForSeconds(1);
            texthandler.UpdateStats();
            yield return new WaitForSeconds(3);
            playerAnimator.ResetTrigger("Attack");
            gameManager.EnemyTurn();
        }
        else
        {
            //add block animation
            yield return new WaitForSeconds(3);
            Debug.Log("EnemyBlocked");
            gameManager.EnemyTurn();
        }
    }
    public IEnumerator PlayerHeal()
    {
        playerAnimator.SetTrigger("Heal");
        yield return new WaitForSeconds(1.5f);
        SpawnHealParticles();
        playerHp = playerHp + Random.Range(0, 15);
        gameManager.playerHP = playerHp;
        if (playerHp > 100)
        {
            playerHp = 100;
        }
        playerStamina -= 10;
        texthandler.UpdateStats();
        yield return new WaitForSeconds(2.5f);
        playerAnimator.ResetTrigger("Heal");
        gameManager.state = BattleState.EnemyTurn;
        gameManager.EnemyTurn();
    }
    public bool playerBlock()
    {
        if (Random.Range(0, 100) > 60)
        {
            Debug.Log("BlockSucsesfull");
            return true;
        }
        else
        {
            Debug.Log("BlockFailed");
            return false;
        }
    }
    public IEnumerator PlayerBlock()
    {
        //play animation
        if (playerBlock())
        {
            gameManager.playerBlocked = true;
        }
        else
        {
            gameManager.playerBlocked = false;
        }
        texthandler.UpdateStats();
        yield return new WaitForSeconds(4);
        gameManager.state = BattleState.EnemyTurn;
        gameManager.EnemyTurn();
    }
    public void SpawnHealParticles()
    {
        GameObject particleSystem = Instantiate(particlePrefab, new Vector3(transform.position.x, transform.position.y, transform.position.z - 1), transform.rotation);
        Destroy(particleSystem, 4);
    }


}
