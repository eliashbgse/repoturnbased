﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;


public class TextHandler : MonoBehaviour
{
    GameManager gameManager;

    float enemyStamina;
    public Slider[] sliders;
    void Start()
    {
        gameManager = transform.parent.gameObject.GetComponent<GameManager>();
    }

    // Update is called once per frame
    public void UpdateStats()
    {
        sliders[2].value = gameManager.enemyHP;
        sliders[3].value = gameManager.enemyStamina;
        sliders[0].value = gameManager.playerHP;
        sliders[1].value = gameManager.playerStamina;
        Debug.Log("StatsUpdated: " + gameManager.enemyHP);
    }
}
